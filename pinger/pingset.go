package pinger

import (
	"log"
	"sync"
	"time"

	"git.slxh.eu/prometheus/ping"
)

// PingSet is a set of pingers
type PingSet struct {
	sync.Mutex
	Interval, TimeLimit time.Duration
	pingers             []*ping.Pinger
	recvHook            func(pkt *ping.Packet)
	wg                  sync.WaitGroup
}

// NewPingSet creates a new PingMap
func NewPingSet(interval, timeLimit time.Duration, recvHook func(pkt *ping.Packet)) *PingSet {
	return &PingSet{
		Interval:  interval,
		TimeLimit: timeLimit,
		recvHook:  recvHook,
		pingers:   make([]*ping.Pinger, 0),
	}
}

// Add adds a new pinger for an address.
// The pinger is started immediately.
func (ps *PingSet) Add(host, source string) (p *ping.Pinger, err error) {
	ps.Lock()
	defer ps.Unlock()

	// Resolve source addresses with preference for IPv6
	srcAddr, _ := ResolveIPAddr("ip", source)

	// Resolve destination with preference for IPv6
	ipAddr, err := ResolveIPAddr(getNetwork(srcAddr), host)
	if err != nil {
		return
	}

	// Create the pinger
	p = &ping.Pinger{
		IPAddr:     ipAddr,
		Host:       host,
		Source:     source,
		SourceAddr: srcAddr,
		Interval:   ps.Interval,
		TimeLimit:  ps.TimeLimit,
		Size:       ping.DefaultSize,
		OnReceive:  observePacket,
		Statistics: ping.NewStatistics(host, ipAddr),
	}
	p.Statistics.Source = source

	// Add pinger to set
	ps.pingers = append(ps.pingers, p)

	// Run the ping function, and delete the pinger after it is done
	ps.wg.Add(1)
	go func() {
		defer ps.wg.Done()
		err := p.Run()
		if err != nil {
			log.Printf("Error sending pings to %s: %s", p.Host, err)
		}
	}()

	return
}

// Stop stops all pingers
func (ps *PingSet) Stop() {
	ps.Lock()
	defer ps.Unlock()

	for _, p := range ps.pingers {
		func() {
			p.Stop()
		}()
	}
	ps.wg.Wait()
}

// Statistics retrieves all statistics
func (ps *PingSet) Statistics() (s []*ping.Statistics) {
	ps.Lock()
	defer ps.Unlock()

	s = make([]*ping.Statistics, len(ps.pingers))
	for i, p := range ps.pingers {
		s[i] = p.Statistics
	}
	return
}

// Hosts retrieves all addresses
func (ps *PingSet) Hosts() (a []string) {
	ps.Lock()
	defer ps.Unlock()

	a = make([]string, len(ps.pingers))
	for i, p := range ps.pingers {
		a[i] = p.Host
	}
	return
}
