package pinger

import (
	"log"
	"math"
	"net"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

// Descriptions used by the collector
var (
	labels = []string{"target", "source"}
	packetsRecvDesc = prometheus.NewDesc(
		"ping_received_packets",
		"Number of packets received",
		labels, nil)
	packetsSentDesc = prometheus.NewDesc(
		"ping_sent_packets",
		"Number of packets sent",
		labels, nil)
	packetLossDesc = prometheus.NewDesc(
		"ping_packet_loss_percent",
		"Packet loss in percent",
		labels, nil)
	avgRttDesc = prometheus.NewDesc(
		"ping_avg_rtt_seconds",
		"Round trip time in seconds",
		labels, nil)
	minRttDesc = prometheus.NewDesc(
		"ping_min_rtt_seconds",
		"Round trip time in seconds",
		labels, nil)
	maxRttDesc = prometheus.NewDesc(
		"ping_max_rtt_seconds",
		"Round trip time in seconds",
		labels, nil)
	stdDevRttDesc = prometheus.NewDesc(
		"ping_stdev_rtt_seconds",
		"Round trip time in seconds",
		labels, nil)
	icmpVersionDesc = prometheus.NewDesc(
		"ping_icmp_version",
		"ICMP version used",
		labels, nil)
)

// Exporter represents the metric exporter configuration.
// It implements prometheus.Collector.
type Exporter struct {
	pingSet             *PingSet
	interval, timeLimit time.Duration
}

// NewExporter creates a metric exporter with the given options
func NewExporter(interval, timeLimit time.Duration) (*Exporter, error) {
	return &Exporter{
		interval:  interval,
		timeLimit: timeLimit,
		pingSet:   NewPingSet(interval, timeLimit, observePacket),
	}, nil
}

// Add adds a new set of pingers
func (e *Exporter) Add(interval, timeLimit, source string, addrs ...string) {
	// Parse durations
	iv, err := time.ParseDuration(interval)
	if err != nil {
		iv = e.interval
	}
	tl, err := time.ParseDuration(timeLimit)
	if err != nil {
		tl = e.timeLimit
	}

	// Create pingers
	e.pingSet = NewPingSet(iv, tl, observePacket)
	for _, a := range addrs {
		_, err = e.pingSet.Add(a, source)
		if err != nil {
			log.Printf("Error creating pinger from %q to %q: %s", source, a, err)
		}
	}
}

// Describe all the metrics exported by the exporter.
// It implements prometheus.Collector.
func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(e, ch)
}

// Collect fetches the stats from configured log files and delivers them
// as Prometheus metrics. It implements prometheus.Collector.
func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
	for _, s := range e.pingSet.Statistics() {
		if math.IsNaN(s.PacketLoss()) {
			continue
		}

		recv, sent := s.Counters()
		min, mean, max := s.Durations()

		ch <- prometheus.MustNewConstMetric(
			packetsRecvDesc, prometheus.GaugeValue, float64(recv), s.Host, s.Source)
		ch <- prometheus.MustNewConstMetric(
			packetsSentDesc, prometheus.GaugeValue, float64(sent), s.Host, s.Source)
		ch <- prometheus.MustNewConstMetric(
			packetLossDesc, prometheus.GaugeValue, 100 * s.PacketLoss(), s.Host, s.Source)
		ch <- prometheus.MustNewConstMetric(
			avgRttDesc, prometheus.GaugeValue, mean.Seconds(), s.Host, s.Source)
		ch <- prometheus.MustNewConstMetric(
			minRttDesc, prometheus.GaugeValue, min.Seconds(), s.Host, s.Source)
		ch <- prometheus.MustNewConstMetric(
			maxRttDesc, prometheus.GaugeValue, max.Seconds(), s.Host, s.Source)
		ch <- prometheus.MustNewConstMetric(
			stdDevRttDesc, prometheus.GaugeValue, s.StandardDeviation().Seconds(), s.Host, s.Source)
		ch <- prometheus.MustNewConstMetric(
			icmpVersionDesc, prometheus.GaugeValue, float64(ipVersion(s.IPAddr.IP)), s.Host, s.Source)
	}
}

// Targets returns the current set of targets
func (e *Exporter) Targets() []string {
	return e.pingSet.Hosts()
}

// Stop stops the exporter
func (e *Exporter) Stop() {
	e.pingSet.Stop()
}

func ipVersion(ip net.IP) int {
	if ip.To4() != nil {
		return 4
	}
	return 6
}
