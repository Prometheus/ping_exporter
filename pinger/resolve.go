package pinger

import (
	"context"
	"net"
)

// ResolveIPAddr is a version of net.ResolveIPAddr with preference for IPv6
func ResolveIPAddr(network, address string) (*net.IPAddr, error) {
	switch network {
	case "ip":
		addrs, err := net.DefaultResolver.LookupIPAddr(context.Background(), address)
		if err != nil {
			return nil, err
		}
		return firstIPv6(addrs), nil
	default:
		return net.ResolveIPAddr(network, address)
	}
}

// firstIPv6 returns the first IPv6 address,
// or the first address of any kind.
func firstIPv6(addrs []net.IPAddr) *net.IPAddr {
	for _, addr := range addrs {
		if addr.IP.To4() == nil {
			return &addr
		}
	}
	return &addrs[0]
}

func getNetwork(addr *net.IPAddr) string {
	switch {
	case addr == nil :
		return "ip"
	case addr.IP.To4() != nil:
		return "ip4"
	default:
		return "ip6"
	}
}
