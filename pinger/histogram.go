package pinger

import (
	"time"

	"git.slxh.eu/prometheus/ping"
	"github.com/prometheus/client_golang/prometheus"
)

const (
	histogramStart  = 500 * time.Microsecond
	histogramFactor = 1.28
	histogramCount  = 24
)

var Histogram = prometheus.NewHistogramVec(
	prometheus.HistogramOpts{
		Name:    "ping_rtt_seconds",
		Help:    "Round trip time in seconds",
		Buckets: prometheus.ExponentialBuckets(histogramStart.Seconds(), histogramFactor, histogramCount),
	},
	labels,
)

func init() {
	prometheus.MustRegister(Histogram)
}

func observePacket(pkt *ping.Packet) {
	Histogram.WithLabelValues(pkt.Host, pkt.Source).Observe(pkt.RTT.Seconds())
}
