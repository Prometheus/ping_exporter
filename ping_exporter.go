package main

import (
	"flag"
	"log"
	"net/http"
	"sync"
	"time"

	"git.slxh.eu/prometheus/ping_exporter/pinger"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type Prometheus struct {
	sync.Mutex
	Exporter *pinger.Exporter
	Registry *prometheus.Registry
}

func NewPrometheus(interval, timeLimit time.Duration, registry *prometheus.Registry) (*Prometheus, error) {
	exporter, err := pinger.NewExporter(interval, timeLimit)
	if err != nil {
		return nil, err
	}

	p := &Prometheus{
		Exporter: exporter,
		Registry: registry,
	}
	err = p.Registry.Register(exporter)
	return p, err
}

var (
	registries = make(map[string]*Prometheus, 1)
	mutex      sync.Mutex
	interval   time.Duration
	timeLimit  time.Duration
)

// diff returns values from `a` that are not in `b`
func diff(a, b []string) (c []string) {
	for _, s1 := range a {
		var m bool
		for _, s2 := range b {
			if s1 == s2 {
				m = true
				break
			}
		}
		if !m {
			c = append(c, s1)
		}
	}
	return
}

func handler(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	// Get the exporter, or create if not exists
	query := r.URL.Query()
	id := query.Get("id")
	mutex.Lock()
	prom, ok := registries[id]
	if !ok {
		var err error
		prom, err = NewPrometheus(interval, timeLimit, prometheus.NewPedanticRegistry())
		if err != nil {
			log.Printf("Error creating Registry %q: %s", id, err)
			return
		}
		registries[id] = prom
	}
	mutex.Unlock()

	// Get all metrics
	promhttp.HandlerFor(prom.Registry, promhttp.HandlerOpts{}).ServeHTTP(w, r)

	// Stop pinging
	prom.Exporter.Stop()

	// Start requested ping threads
	if targets, ok := query["target"]; ok {
		// Remove old values from histogram
		for _, t := range diff(prom.Exporter.Targets(), targets) {
			pinger.Histogram.DeleteLabelValues(t)
		}

		// Create pingers
		prom.Exporter.Add(query.Get("interval"), query.Get("timelimit"), query.Get("source"), targets...)
	}
}

func main() {
	var addr, metricsPath string
	var err error

	flag.StringVar(&addr, "addr", ":9145", "Address and port to listen on")
	flag.StringVar(&metricsPath, "metrics-path", "/metrics", "Path under which to expose metrics")
	flag.DurationVar(&interval, "interval", 5 * time.Second, "Default ping interval")
	flag.DurationVar(&timeLimit, "time-limit", 10 * time.Minute, "Default ping time limit")
	flag.Parse()

	log.Printf("Ping interval: %s, ping time limit: %s", interval, timeLimit)

	// Create the default collector
	registries[""], err = NewPrometheus(interval, timeLimit, prometheus.DefaultRegisterer.(*prometheus.Registry))
	if err != nil {
		log.Fatalf("Error creating default exporter: %s", err)
	}
	defer stopExporters()

	// Register
	http.HandleFunc(metricsPath, handler)

	// Listen
	log.Println("Listening on", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}

func stopExporters() {
	for _, p := range registries {
		p.Exporter.Stop()
	}
}
